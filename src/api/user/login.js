import axios from "../instance";

export default {
  login: (body) => {
    return axios.api
      .post("/users/adminLogin", body)
      .then((response) => response.data);
  },
};
