import axios from "../instance";
import addQuery from "../../utils/addQuery";

export default {
  getAll: async (payload) => {
    const result = await addQuery(payload);

    return axios.api
      .get(`/customer?${result}`)
      .then((response) => response.data);
  },
  getOne: (id) => {
    return axios.api.get(`/customer/${id}`).then((response) => response.data);
  },
  search: async (body, payload) => {
    const result = await addQuery(payload);

    return axios.api
      .post(`/customer/search?${result}`, body)
      .then((response) => response.data);
  },
  create: (body) => {
    return axios.api.post("/customer", body).then((response) => response.data);
  },
  update: (id, body) => {
    return axios.api
      .put(`/customer/${id}`, body)
      .then((response) => response.data);
  },
  delete: (id) => {
    return axios.api
      .delete(`/customer/${id}`)
      .then((response) => response.data);
  },
};
