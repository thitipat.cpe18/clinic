import customer from "./customer";
import salesOrder from "./salesOrder";
import pos from "./pos";

export default {
  customer,
  salesOrder,
  pos,
};
