import transferee from "./transferee";
import transferProduct from "./transferProduct";

export default {
  transferee,
  transferProduct,
};
