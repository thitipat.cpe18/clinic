import vendor from "./vendor";
import purchaseOrder from "./purchaseOrder";

export default {
  vendor,
  purchaseOrder,
};
