import sale from "./sale";
import product from "./product";

export default {
  sale,
  product,
};
