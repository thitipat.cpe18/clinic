const DocMenuConfig = [
  {
    pages: [
      {
        heading: "dashboard",
        route: "/dashboard",
        svgIcon: "media/icons/duotune/general/gen025.svg",
      },
      {
        sectionTitle: "จัดการระบบ",
        route: "/manage-system",
        svgIcon: "media/icons/duotune/ecommerce/ecm007.svg",
        sub: [
          {
            heading: "กำหนดงวดบัญชี : OP1",
            route: "/manage-system/SetAccountPeriod",
          },
          {
            heading: "ปรับปรุงยอดยกมา : OP2",
            route: "/manage-system/invoice",
          },
          {
            heading: "คำนวณสร้างยอดยกไป OP3",
            route: "/manage-system/faq",
          },
          // {
          //   heading: "wizard",
          //   route: "/pages/wizard",
          // },
          // {
          //   heading: "pricing",
          //   route: "/pages/pricing",
          // },
        ],
      },
      {
        sectionTitle: "คลังสินค้า",
        route: "/account",
        svgIcon: "media/icons/duotune/communication/com006.svg",
        sub: [
          {
            heading: "คลังสินค้า : WH1",
            route: "/account/overview",
          },
          {
            heading: "เพิ่มกลุ่มสินค้าหลัก : WH2",
            route: "/account/settings",
          },
          {
            heading: "เพิ่มกลุ่มสินค้ารอง : WH3",
            route: "/account/security",
          },
          {
            heading: "Product Library : WH4",
            route: "/account/audit-logs",
          },
          {
            heading: "จัดการสินค้าหมดอายุ : WH5",
            route: "/account/activity",
          },
          {
            heading: "ตัดสินค้าหมดอายุ / ชำรุด / เสียหาย (W6-1)",
            route: "/account/activity",
          },
          {
            heading: "อนุมัติตัดสินค้าหมดอายุ / ชำรุด / เสียหาย (WH6-2)",
            route: "/account/activity",
          },
          {
            heading: "Report : WH7",
            route: "/account/activity",
          },
        ],
      },
      {
        sectionTitle: "ซื้อ",
        route: "/authentication",
        svgIcon: "media/icons/duotune/technology/teh004.svg",
        sub: [
          {
            heading: "เพิ่มผู้ขาย / ผู้รับเงิน : PU1",
            route: "/account/overview",
          },
          {
            heading: "สร้างใบสั่งซื้อสินค้า : PU2-1",
            route: "/account/settings",
          },
          {
            heading: "อนุมัติใบสั่งซื้อสินค้า : PU2-2",
            route: "/account/security",
          },
          {
            heading: "รับสินค้า - รอดำเนินการ",
            route: "/account/audit-logs",
          },
          {
            heading: "รับสินค้า - เข้าคลังแล้ว : PU2-4",
            route: "/account/activity",
          },
          //   {
          //     sectionTitle: "base",
          //     route: "/base",
          //     sub: [
          //       {
          //         heading: "signIn",
          //         route: "/authentication/base/sign-in",
          //       },
          //       {
          //         heading: "signUp",
          //         route: "/authentication/base/sign-up",
          //       },
          //       // {
          //       //   heading: "twoStepsAuth",
          //       //   route: "/authentication/base/two-steps-auth",
          //       // },
          //       {
          //         heading: "passwordReset",
          //         route: "/authentication/base/password-reset",
          //       },
          //       // {
          //       //   heading: "newPassword",
          //       //   route: "/authentication/base/new-password",
          //       // },
          //     ],
          //   },
          //   {
          //     sectionTitle: "extended",
          //     route: "/extended",
          //     sub: [
          //       {
          //         heading: "multiStep",
          //         route: "/authentication/extended/multi-steps",
          //       },
          //       // {
          //       //   heading: "freeTrial",
          //       //   route: "/authentication/extended/free-trial",
          //       // },
          //       {
          //         heading: "comingSoon",
          //         route: "/authentication/extended/coming-soon",
          //       },
          //     ],
          //   },
          //   {
          //     sectionTitle: "general",
          //     route: "/general",
          //     sub: [
          //       {
          //         heading: "welcome",
          //         route: "/authentication/general/welcome",
          //       },
          //       {
          //         heading: "verifyEmail",
          //         route: "/authentication/general/verify-email",
          //       },
          //       {
          //         heading: "passwordConfirmation",
          //         route: "/authentication/general/password-confirmation",
          //       },
          //       {
          //         heading: "closeAccount",
          //         route: "/authentication/general/close-account",
          //       },
          //       {
          //         heading: "error404",
          //         route: "/authentication/general/404",
          //       },
          //       {
          //         heading: "error500",
          //         route: "/authentication/general/500",
          //       },
          //     ],
          //   },
          //   {
          //     sectionTitle: "email",
          //     route: "/email",
          //     sub: [
          //       {
          //         heading: "verifyEmail",
          //         route: "/authentication/email/verify-email",
          //       },
          //       {
          //         heading: "passwordReset",
          //         route: "/authentication/email/password-reset",
          //       },
          //       {
          //         heading: "passwordChange",
          //         route: "/authentication/email/password-change",
          //       },
          //     ],
          //   },
        ],
      },
      {
        sectionTitle: "ขาย",
        route: "/subscriptions",
        svgIcon: "media/icons/duotune/ecommerce/ecm002.svg",
        sub: [
          {
            heading: "เพิ่มผู้ซื้อ / ผู้จ่ายเงิน : SA1",
            route: "/subscriptions/getting-started",
          },
          {
            heading: "สร้างใบขายส่ง (S2-1)",
            route: "/subscriptions/subscription-list",
          },
          {
            heading: "อนุมัติขายส่ง : SA2-2",
            route: "/subscriptions/add-subscription",
          },
          {
            heading: "สร้างใบขายปลีก : SA3-1",
            route: "/subscriptions/view-subscription",
          },
          {
            heading: "อนุมัติขายปลีก : SA3-2",
            route: "/subscriptions/view-subscription",
          },
          {
            heading: "สร้างใบโอนสินค้าออก : SA4-1",
            route: "/subscriptions/view-subscription",
          },
          {
            heading: "อนุมัติโอนสินค้าออก : SA4-2",
            route: "/subscriptions/view-subscription",
          },
          {
            heading: "POS : SAS",
            route: "/subscriptions/view-subscription",
          },
          {
            heading: "สรุปขายปลีกประจำวัน",
            route: "/subscriptions/view-subscription",
          },
          {
            heading: "Price tag",
            route: "/subscriptions/view-subscription",
          },
        ],
      },
      // {
      //   sectionTitle: "chat",
      //   route: "/chat",
      //   svgIcon: "media/icons/duotune/communication/com012.svg",
      //   sub: [
      //     {
      //       heading: "privateChat",
      //       route: "/chat/private-chat",
      //     },
      //     {
      //       heading: "groupChat",
      //       route: "/chat/group-chat",
      //     },
      //     {
      //       heading: "drawerChat",
      //       route: "/chat/drawer-chat",
      //     },
      //   ],
      // },
      // {
      //   heading: "calendar",
      //   route: "/calendar",
      //   svgIcon: "media/icons/duotune/general/gen014.svg",
      // },
    ],
  },
];

export default DocMenuConfig;
