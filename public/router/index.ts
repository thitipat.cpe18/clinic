import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/dashboard",
    component: () => import("@/layout/Layout.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/views/Dashboard.vue"),
      },
      // {
      //   path: "/builder",
      //   name: "builder",
      //   component: () => import("@/views/Builder.vue"),
      // },
      {
        path: "/manage-system/SetAccountPeriod",
        name: "set-account-period",
        component: () => import("@/views/manage-system/SetAccountPeriod.vue"),
      },
      {
        path: "/manage-system/invoice",
        name: "invoice",
        component: () => import("@/views/manage-system/Invoice.vue"),
      },
      {
        path: "/manage-system/faq",
        name: "faq",
        component: () => import("@/views/manage-system/FAQ.vue"),
      },
      {
        path: "/manage-system/wizard",
        name: "wizard",
        component: () => import("@/views/manage-system/Wizard.vue"),
      },
      {
        path: "/manage-system/pricing",
        name: "pricing",
        component: () => import("@/views/manage-system/Pricing.vue"),
      },
      {
        path: "/account",
        name: "account",
        component: () => import("@/components/page-layouts/Account.vue"),
        children: [
          {
            path: "overview",
            name: "account-overview",
            component: () => import("@/views/account/Overview.vue"),
          },
          {
            path: "settings",
            name: "account-settings",
            component: () => import("@/views/account/Settings.vue"),
          },
          {
            path: "security",
            name: "account-security",
            component: () => import("@/views/account/Security.vue"),
          },
          {
            path: "audit-logs",
            name: "account-audit-logs",
            component: () => import("@/views/account/AuditLogs.vue"),
          },
          {
            path: "activity",
            name: "account-activity",
            component: () => import("@/views/account/Activity.vue"),
          },
        ],
      },
      {
        path: "/chat/private-chat",
        name: "private-chat",
        component: () => import("@/views/chat/Chat.vue"),
      },
      {
        path: "/chat/group-chat",
        name: "group-chat",
        component: () => import("@/views/chat/Chat.vue"),
      },
      {
        path: "/chat/drawer-chat",
        name: "drawer-chat",
        component: () => import("@/views/chat/DrawerChat.vue"),
      },
      {
        path: "/subscriptions/getting-started",
        name: "subscriptions-getting-started",
        component: () => import("@/views/subscriptions/GettingStarted.vue"),
      },
      {
        path: "/subscriptions/subscription-list",
        name: "subscriptions-subscription-list",
        component: () => import("@/views/subscriptions/SubscriptionList.vue"),
      },
      {
        path: "/subscriptions/add-subscription",
        name: "subscriptions-add-subscription",
        component: () => import("@/views/subscriptions/AddSubscription.vue"),
      },
      {
        path: "/subscriptions/view-subscription",
        name: "subscriptions-view-subscription",
        component: () => import("@/views/subscriptions/ViewSubscription.vue"),
      },
      {
        path: "/calendar",
        name: "calendar",
        component: () => import("@/views/Calendar.vue"),
      },
    ],
  },
  {
    path: "/authentication/base",
    name: "authentication-base",
    component: () => import("@/components/page-layouts/AuthenticationBase.vue"),
    children: [
      {
        path: "sign-in",
        name: "authentication-base-sign-in",
        component: () => import("@/views/authentication/base/SignIn.vue"),
      },
      {
        path: "sign-up",
        name: "authentication-base-sign-up",
        component: () => import("@/views/authentication/base/SignUp.vue"),
      },
      {
        path: "two-steps-auth",
        name: "authentication-base-two-steps-auth",
        component: () => import("@/views/authentication/base/TwoStepsAuth.vue"),
      },
      {
        path: "password-reset",
        name: "authentication-base-password-reset",
        component: () =>
          import("@/views/authentication/base/PasswordReset.vue"),
      },
      {
        path: "new-password",
        name: "authentication-base-new-password",
        component: () => import("@/views/authentication/base/NewPassword.vue"),
      },
    ],
  },
  {
    path: "/authentication/extended/multi-steps",
    name: "authentication-extended-multi-steps",
    component: () => import("@/views/authentication/extended/MultiSteps.vue"),
  },
  {
    path: "/authentication/extended/free-trial",
    name: "authentication-extended-free-trial",
    component: () => import("@/views/authentication/extended/FreeTrial.vue"),
  },
  {
    path: "/authentication/extended/coming-soon",
    name: "authentication-extended-coming-soon",
    component: () => import("@/views/authentication/extended/ComingSoon.vue"),
  },
  {
    path: "/authentication/general",
    name: "authentication-general",
    component: () =>
      import("@/components/page-layouts/AuthenticationGeneral.vue"),
    children: [
      {
        path: "welcome",
        name: "authentication-general-welcome",
        component: () => import("@/views/authentication/general/Welcome.vue"),
      },
      {
        path: "verify-email",
        name: "authentication-general-verify-email",
        component: () =>
          import("@/views/authentication/general/VerifyEmail.vue"),
      },
      {
        path: "password-confirmation",
        name: "authentication-general-password-confirmation",
        component: () =>
          import("@/views/authentication/general/PasswordConfirmation.vue"),
      },
      {
        path: "close-account",
        name: "authentication-general-close-account",
        component: () =>
          import("@/views/authentication/general/CloseAccount.vue"),
      },
      {
        path: "404",
        name: "authentication-general-404",
        component: () => import("@/views/authentication/general/Error404.vue"),
      },
      {
        path: "500",
        name: "authentication-general-500",
        component: () => import("@/views/authentication/general/Error500.vue"),
      },
    ],
  },
  {
    path: "/authentication/email",
    name: "authentication-email",
    component: () =>
      import("@/components/page-layouts/AuthenticationEmail.vue"),
    children: [
      {
        path: "verify-email",
        name: "authentication-email-verify-email",
        component: () => import("@/views/authentication/email/VerifyEmail.vue"),
      },
      {
        path: "password-reset",
        name: "authentication-email-password-reset",
        component: () =>
          import("@/views/authentication/email/PasswordReset.vue"),
      },
      {
        path: "password-change",
        name: "authentication-email-password-change",
        component: () =>
          import("@/views/authentication/email/PasswordChange.vue"),
      },
    ],
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/authentication/general/404",
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach(() => {
  // Scroll page to top on every route change
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

export default router;
